package app

import (
	"gitlab.com/Dissareezy/golang/controllers"
	"log"
	"net/http"
)

func StartApp() {
	http.HandleFunc("/users", controllers.GetUser)
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)

	}
}
