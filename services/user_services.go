package services

import (
	"gitlab.com/Dissareezy/golang/domain"
	"gitlab.com/Dissareezy/golang/utils"
)

func GetUser(userid int64) (*domain.User, *utils.ApplicationError) {
	return domain.GetUser(userid)
}
