package domain

import (
	"gitlab.com/Dissareezy/golang/utils"
	"net/http"
)

var users = map[int64]*User{

	13: {
		Id:        13,
		FirtsName: "Shyngys",
		LastName:  "Muratkhanov",
		Email:     "murathanov_2002@mail.ru",
	},
	12: {
		Id:        12,
		FirtsName: "Imangali",
		LastName:  "Kalybek",
		Email:     "imangalikalybek@gmail.com",
	},
}

func GetUser(userId int64) (*User, *utils.ApplicationError) {
	if user := users[userId]; user != nil {
		return user, nil
	}
	return nil, &utils.ApplicationError{
		Message: "user not found",
		Status:  http.StatusNotFound,
		Code:    "not_found",
	}
}
